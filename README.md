# One Base

Basic Ansible Setup for a server with Traefik, Portainer, Autoheal and Watchtower.

Prerequisites
* GIT https://git-scm.com/
* Ansbile https://www.ansible.com/

Clone this repository with submodules
```sh
git clone --recurse-submodules git@gitlab.com:waldorf.one/one-base.git
```
Enter the directory `cd one-base`.

### Install Ansible requirements

```sh
ansible-galaxy install -r requirements.yml
```

## Add a host

You need to setup your hosts with a user to perform `sudo`.
Place your SSH key on the server and login once.

```sh
cp host_vars/192.168.0.0.yml host_vars/YOUR.IP.HE.RE.yml
sed -i 's/192\.168\.0\.0/YOUR\.IP\.HE\.RE/g' hosts
```
And update the files contents as needed.

## Run the playbook

```sh
ansible-playbook site.yml -i hosts --ask-become-pass
```

